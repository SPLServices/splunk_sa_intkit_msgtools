#!/usr/bin/env python
# coding=utf-8
#
# Copyright © 2011-2015 Splunk, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License"): you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

from __future__ import absolute_import, division, print_function, unicode_literals
import app

from splunklib.searchcommands import dispatch, StreamingCommand, Configuration, Option, validators
import os,sys,time
import json


@Configuration()
class GenerateJSONMSGCommand(StreamingCommand):
    """ Counts the number of non-overlapping matches to a regular expression in a set of fields.
    ##Syntax
    .. code-block::
        generatejsonmsg suppress_empty=Boolean suppress_unknown=Boolean suppress_stringnull=Boolean output_field=<field>
        include_metadata=Boolean include_fields=Boolean include_raw=Boolean sort_fields=Boolean sort_mv=boolean
    ##Description
        Creates a new field with a json formated string containing the event based on the params selected
    ##Example
       Create a message using the default field _msg containing all attributes available from the All Traffic
       model.
    .. code-block::
        | datamodel Network_Traffic All_Traffic search | fields + _raw,All_Traffic.* | generatejsonmsg
    """

    suppress_empty = Option(
        doc='''
        **Syntax:** **suppress_empty=***<boolean>*
        **Description:** Do not emit empty or null fields''',
        require=False,default=True, validate=validators.Boolean())

    suppress_unknown = Option(
        doc='''
        **Syntax:** **suppress_unknown=***<boolean>*
        **Description:** Do not emit unknown or null fields''',
        require=False,default=True, validate=validators.Boolean())

    suppress_stringnull = Option(
        doc='''
        **Syntax:** **suppress_stringnull=***<boolean>*
        **Description:** Do not emit fields with value of the text null''',
        require=False,default=True, validate=validators.Boolean())

    output_field = Option(
        doc='''
        **Syntax:** **output_field=***string*
        **Description:** Destination of new message field ''',
        require=False,default="_msg",)

    include_metadata = Option(
        doc='''
        **Syntax:** **include_metadata=***<boolean>*
        **Description:** Include metadata object and fields including _* source sourcetype and index''',
        require=False,default=True, validate=validators.Boolean())

    include_fields = Option(
        doc='''
        **Syntax:** **include_fields=***<boolean>*
        **Description:** Include splunk parsed fields''',
        require=False,default=True, validate=validators.Boolean())

    include_raw = Option(
        doc='''
        **Syntax:** **include_raw=***<boolean>*
        **Description:** Include _raw node ASCII safe will be used to escape unsafe chars''',
        require=False,default=True, validate=validators.Boolean())

    sort_fields = Option(
        doc='''
        **Syntax:** **sort_fields=***<boolean>*
        **Description:** Sort fields for human readability''',
        require=False,default=False, validate=validators.Boolean())

    sort_mv = Option(
        doc='''
        **Syntax:** **sort_mv=***<boolean>*
        **Description:** sort and dedup mv fields''',
        require=False,default=True, validate=validators.Boolean())

    def stream(self, records):
        self.logger.info('GenerateJSONMSGCommand: %s', self)  # logs command line


        for record in records:
		metadata = {}
	        data = {}

                self.logger.debug("building metadata")
                if record.has_key('splunk_server'):
                        self.logger.debug("metadata splunk_server")
                        metadata['splunk_server']=record['splunk_server']

                if record.has_key('linecount'):
                        self.logger.debug("metadata linecount")
                        metadata['linecount']=record['linecount']

                if record.has_key('source'):
                        self.logger.debug("metadata source")
                        metadata['source']=record['source']

                if record.has_key('index'):
                        self.logger.debug("metadata index")
                        metadata['index']=record['index']

                for key, value in record.items():
                        if key.startswith("_") and key!="_raw":
                                metadata[key]=value
                        elif key in metadata:
                                self.logger.debug("metadata key %s",key)
                        elif key == '_raw':
                                self.logger.debug("skip raw")
                        elif isinstance(value, basestring) and self.suppress_unknown and  value == "unknown":
				pass
                        elif isinstance(value, basestring) and self.suppress_empty and (value == "" or len(value) == 0 or value is None):
				pass
                        elif isinstance(value, basestring) and self.suppress_stringnull and (value.lower() == "null"):
				pass
                        else:
                                self.logger.debug("data %s ",key)
                                data[key]=value


               	metadata["_pubtime"] = int(time.time())

	        msg={}
        	if self.include_metadata:
			msg["metadata"]={}
			if self.sort_fields:
	        	        for key in sorted(metadata.iterkeys()):
        	        		msg["metadata"][key]=metadata[key]
			else:
				msg["metadata"]=metadata
	        if self.include_fields:
			msg["fields"]={}
			if self.sort_fields:
	        	        for key in sorted(data.iterkeys()):
        	        	        msg["fields"][key]=data[key]
			else:
				msg["fields"]=data
                if self.include_raw and record.has_key('_raw'):
                        self.logger.debug("raw")
                        msg["raw"]=record['_raw']


            	record[self.output_field] = json.dumps(msg)
		yield record


if __name__ == "__main__":
    dispatch(GenerateJSONMSGCommand, sys.argv, sys.stdin, sys.stdout, __name__)
